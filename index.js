const http = require('http');
const fs = require('fs');
const url = require('url');

const PORT = Number(process.env.PORT) || 8080;
const ERROR_CODE = 400;
const SUCCESS_CODE = 200;
const ERROR_NO_FILE_CODE = 'ENOENT';

function writeLogs(message) {
    const timestamp = Date.now();
    fs.readFile('./logs.json', (err, content) => {
        if (err) {
            if (err.code === ERROR_NO_FILE_CODE) {
                const data = {
                    'logs': []
                };
                data.logs.push({message, timestamp});
                fs.writeFile('./logs.json', JSON.stringify(data), (err) => {
                    if (err) {
                        console.log(`logs wasn't written`);
                    } else {
                        console.log(`logs was written`);
                    }
                })
            } else {
                console.log(`logs wasn't read and written`);
            }
        } else {
            const data = JSON.parse(content);
            data.logs.push({message, timestamp});
            fs.writeFile('./logs.json', JSON.stringify(data), (err) => {
                if (err) {
                    console.log(`logs wasn't written`);
                } else {
                    console.log(`logs was written`);
                }
            })
        }
    });
}

function readLogs(content, from, to) {
    if (!from && !to) {
        return content;
    } else {
        const result = {
            'logs': []
        }                    
        let rangeData = [];
        const data = JSON.parse(content).logs;
        let fromDate, toDate;
        switch (true) {
            case from && !to:
                fromDate = new Date(+from);
                rangeData = data.filter(log => log.timestamp >= fromDate);
                result.logs = rangeData;
                break;
            case to && !from:
                toDate = new Date(+to);
                rangeData = data.filter(log => log.timestamp <= toDate);
                result.logs = rangeData;
                break;
            default:
                fromDate = new Date(+from);
                toDate = new Date(+to);
                rangeData = data.filter(log => log.timestamp >= fromDate && log.timestamp <= toDate);
                result.logs = rangeData;
        }
        return JSON.stringify(result);
    }
}

module.exports = () => {
    http.createServer((req, res) => {
        const { pathname, query } = url.parse(req.url, true);
        if (req.method === 'POST') {
            if (pathname.includes('/file') && query) {
                const { filename, content } = query;
                fs.access('./files', err => {
                    if (err && err.code === ERROR_NO_FILE_CODE) {
                      fs.mkdir('./files', (err) => {
                        if (err) {
                            res.writeHead(ERROR_CODE, {
                                'Content-Type': 'text/html'
                            });
                            res.end(`Directory 'files' doesn't exist`);
                            return;
                        }
                      });
                    }
                    if (filename && fs.existsSync(`./files/${filename}`)) {
                        fs.appendFile(`./files/${filename}`, content || '', async (err) => {
                            if (err) {
                                res.writeHead(ERROR_CODE, {
                                    'Content-Type': 'text/html'
                                });
                                res.end(`Content wasn't written into ${filename}`);
                            } else {
                                await writeLogs(`Content was written into ${filename}`);
                                res.writeHead(SUCCESS_CODE, {
                                    'Content-Type': 'text/html'
                                });
                                res.end(`Content was written into ${filename}`);                                    
                            }
                        })
                    } else if (filename && !fs.existsSync(`./files/${filename}`)) {
                        fs.writeFile(`./files/${filename}`, content || '', async (err) => {
                            if (err) {
                                res.writeHead(ERROR_CODE, {
                                    'Content-Type': 'text/html'
                                });
                                res.end(`Content wasn't written into ${filename}`);
                            } else {
                                await writeLogs(`File ${filename} was created and content was written into it`)
                                res.writeHead(SUCCESS_CODE, {
                                    'Content-Type': 'text/html'
                                });
                                res.end(`${filename} was created and written`);                                    
                            }
                        })
                    } else {
                        res.writeHead(ERROR_CODE, {
                            'Content-Type': 'text/html'
                        });
                        res.end('Filename shouldn\'t be empty');
                    }
                });
            } else {
                res.writeHead(ERROR_CODE, {
                    'Content-Type': 'text/html'
                });
                res.end('Something went wrong');                
            }
        } else if (req.method === 'GET') {
            if (pathname === '/logs') {
                const { from, to } = query;
                fs.readFile('./logs.json', (err, content) => {
                    if (err) {
                        res.writeHead(ERROR_CODE, {
                            'Content-Type': 'text/html'
                        });
                        if (err.code === ERROR_NO_FILE_CODE) {
                            res.end('No logs file');
                        } else {
                            res.end('Something went wrong');
                        }
                    } else {
                        res.writeHead(SUCCESS_CODE, {
                            'Content-Type': 'application/json'
                        });
                        res.end(readLogs(content, from, to)); 
                    }
                });
            } else if (pathname.includes('/file')) {
                const [ , , file ] = pathname.split('/');
                fs.readFile(`./files/${file}`, async (err, content) => {
                    if (err) {
                        res.writeHead(ERROR_CODE, {
                            'Content-Type': 'text/html'
                        });
                        if (err.code === ERROR_NO_FILE_CODE) {
                            res.end(`No files with ${file}`);
                        } else {
                            res.end('Something went wrong.');
                        }
                        return;
                    }
                    await writeLogs(`File with name ${file} was read.`);
                    res.writeHead(SUCCESS_CODE, {
                        'Content-Type': 'text/html'
                    });
                    res.end(content); 
                })                 
            } else {
                res.writeHead(SUCCESS_CODE, {
                    'Content-Type': 'text/html'
                });
                res.end('Hello NodeJS');
            }
        } else {
            res.writeHead(ERROR_CODE, {
                'Content-Type': 'text/html'
            });
            res.end(`Method ${req.method} is not exist`); 
        }
    }).listen(PORT, () => console.log('Server has been started'));
}